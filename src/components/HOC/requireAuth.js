import React, { Component } from "react";
import { connect } from "react-redux";

export default ChildComponent => {
    class ComposedComponent extends Component {
        denyAccess = () => {
            if (!this.props.auth) {
                return <h4>Please log in to see or manage streams.</h4>;
            } else {
                return <ChildComponent {...this.props} />;
            }
        };

        render() {
            return this.denyAccess();
        }
    }

    const mapStateToProps = state => {
        return {
            auth: state.auth
        };
    };

    return connect(mapStateToProps)(ComposedComponent);
};
