import _ from "lodash";
import {
  GET_STREAMS_LIST,
  ADD_NEW_STREAM,
  DELETE_STREAM
} from "../actions/actionTypes";

const streamsReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_STREAMS_LIST:
      return { ...state, ...action.payload };
    case ADD_NEW_STREAM:
      console.log("payload", action.payload);
      return { ...state, ...action.payload };
    case DELETE_STREAM:
      const newList = _.omit(state, [action.payload]);
      return newList;
    default:
      return state;
  }
};

export default streamsReducer;
