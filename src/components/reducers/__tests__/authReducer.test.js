import authReducer from "../authReducer";

describe("Auth Reducer", () => {
    it("returns the same state when a non-matching action is passed", () => {
        const newState = authReducer(true, {});
        expect(newState).toEqual(true);
    });

    it("returns the new state when AUTH_CHANGE type is passed", () => {
        const newState = authReducer(false, {
            type: "AUTH_CHANGE",
            payload: true
        });
        expect(newState).toEqual(true);
    });
});
