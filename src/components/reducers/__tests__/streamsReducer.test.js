import streamsReducer from "../streamsReducer";
import {
    GET_STREAMS_LIST,
    ADD_NEW_STREAM,
    DELETE_STREAM
} from "../../actions/actionTypes";

describe("Streams Reducer", () => {
    it("returns initial state when type doesn't match", () => {
        const newState = streamsReducer({}, {});
        expect(newState).toEqual({});
    });

    it("return streams list when passes  GET_STREAMS_LIST action", () => {
        const payload = { 2: { id: "2", name: "Amal", duration: "50" } };
        const newState = streamsReducer(
            {},
            {
                type: GET_STREAMS_LIST,
                payload: payload
            }
        );
        expect(newState).toEqual({ ...payload });
    });

    it("adds a new stream to the list when passes ADD_NEW_STREAM action", () => {
        const initialState = {
            1: { id: "1", name: "Arun", duration: "20" },
            3: { id: "3", name: "Ajit", duration: "30" }
        };
        const payload = { 2: { id: "2", name: "Amal", duration: "50" } };
        const newState = streamsReducer(initialState, {
            type: ADD_NEW_STREAM,
            payload: payload
        });
        expect(newState).toEqual({ ...initialState, ...payload });
    });

    it("deleted a stream from the list when passes  DELETE_STREAM action", () => {
        const initialState = {
            1: { id: "1", name: "Arun", duration: "20" },
            3: { id: "3", name: "Ajit", duration: "30" }
        };
        const payload = "1";
        const newState = streamsReducer(initialState, {
            type: DELETE_STREAM,
            payload: payload
        });
        expect(newState).toEqual({
            3: { id: "3", name: "Ajit", duration: "30" }
        });
    });
});
