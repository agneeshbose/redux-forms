import { combineReducers } from "redux";
import streamsReducer from "./streamsReducer";
import authReducer from "./authReducer";

export default combineReducers({
  streamsList: streamsReducer,
  auth: authReducer
});
