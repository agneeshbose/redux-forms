import { getStreamsList, addNewStream, deleteStream } from "../../api/stream";
import _ from "lodash";
import { GET_STREAMS_LIST, ADD_NEW_STREAM, DELETE_STREAM } from "./actionTypes";

//Action to get the initial stream list from api
export const getStreamsListAction = () => {
    return async dispatch => {
        await getStreamsList()
            .then(res => {
                const streams = _.mapKeys(res.data, "id");
                dispatch({
                    type: GET_STREAMS_LIST,
                    payload: streams
                });
            })
            .catch(() => {
                dispatch({
                    type: GET_STREAMS_LIST,
                    payload: { error: "error" }
                });
            });
    };
};
//

//Action to add new stream to collection in api and update redux state
export const addNewStreamAction = newStream => {
    return async dispatch => {
        await addNewStream(newStream).then(res => {
            const streamData = [_.pick(res.data, ["name", "id", "duration"])];
            dispatch({
                type: ADD_NEW_STREAM,
                payload: _.mapKeys(streamData, "id")
            });
        });
    };
};
//

//Action to delete an existing stream from collection
export const deleteStreamAction = id => {
    return async dispatch => {
        await deleteStream(id).then(res => {
            dispatch({
                type: DELETE_STREAM,
                payload: id
            });
        });
    };
};
//
