export const getAuthStatusAction = payload => {
  return {
    type: "AUTH_CHANGE",
    payload: payload
  };
};
