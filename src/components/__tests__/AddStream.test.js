import React from "react";
import { shallow } from "enzyme";
import { AddStream } from "../streams/AddStream";

describe("AddStream component", () => {
    let wrapped, props;
    beforeEach(() => {
        props = {
            addNewStreamAction: jest.fn(() => "test")
        };
        wrapped = shallow(<AddStream {...props} />);
    });

    it("doesn't show Modal box by default", () => {
        expect(wrapped.state().modalStatus).toBe(false);
    });

    it("shows the modal box when clicks on add stream button", () => {
        wrapped.find(".addstream-button").simulate("click");
        wrapped.update();
        expect(wrapped.state().modalStatus).toBe(true);
    });

    it("calls addNewStreamAction on submitting modal form", () => {
        const instance = wrapped.instance();
        instance.onSubmitModal({});
        expect(props.addNewStreamAction.mock.calls.length).toBe(1);
    });

    it("calls closeAddStreamModal on closing modal", () => {
        const instance = wrapped.instance();
        wrapped.setState({ modalStatus: true });
        instance.closeAddStreamModal();
        expect(wrapped.state().modalStatus).toBe(false);
    });
});
