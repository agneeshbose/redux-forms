import React from "react";
import { shallow } from "enzyme";
import Header from "../Header";

it("shows a google authentication button", () => {
  const wrapped = shallow(<Header />);
  const authButton = `[data-test="auth-button"]`;

  expect(wrapped.find(authButton).length).toEqual(1);
});
