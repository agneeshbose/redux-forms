import React from "react";
import { shallow } from "enzyme";
import { StreamList } from "../streams/StreamList";

describe("StreamsList component testing with store data", () => {
    let wrapped, props;
    beforeEach(() => {
        const streams = {
            1: { id: 1, duration: "4", name: "Four" },
            2: { id: 2, duration: "5", name: "Five" }
        };
        props = {
            getStreamsListAction: jest.fn(() => "test"),
            deleteStreamAction: jest.fn(() => "test"),
            streamsList: streams
        };
        wrapped = shallow(<StreamList {...props} />);
    });

    it("calls getStreamsListAction to get the initial data", () => {
        expect(props.getStreamsListAction).toBeCalled();
    });

    it("renders an add stream button and a table of streams data", () => {
        expect(wrapped.find(".addStreamComponent").length).toEqual(1);
        expect(wrapped.find(".streams-table").length).toEqual(1);
    });

    it("renders same number of rows and delete buttons as number of streams", () => {
        expect(wrapped.find(".data-row").length).toEqual(2);
        expect(wrapped.find(".deletestream-button").length).toEqual(2);
    });

    it("pops up a modal box when clicks on delet stream button", () => {
        expect(wrapped.state().modalStatus).toBe(false);
        wrapped
            .find(".deletestream-button")
            .first()
            .simulate("click");
        expect(wrapped.state().modalStatus).toBe(true);
    });

    it("hides the modal on calling handleClickCancel", () => {
        wrapped.setState({ modalStatus: true });
        const instance = wrapped.instance();
        instance.handleClickCancel();
        expect(wrapped.state().modalStatus).toBe(false);
    });

    it("calls handleDelete and hides the modal box on clicking delete stream in the modal", () => {
        wrapped.setState({ modalStatus: true });
        const instance = wrapped.instance();
        instance.handleDeleteStream(2);
        expect(wrapped.state().modalStatus).toBe(false);
        expect(props.deleteStreamAction).toBeCalled();
    });
});

describe("StreamsList component when data not available", () => {
    let wrapped, props;
    beforeEach(() => {
        props = {
            getStreamsListAction: jest.fn(() => "test"),
            deleteStreamAction: jest.fn(() => "test"),
            streamsList: { error: "error" }
        };
        wrapped = shallow(<StreamList {...props} />);
    });

    it("does not render data table or add stream button", () => {
        expect(wrapped.find(".addStreamComponent").length).toEqual(0);
        expect(wrapped.find(".streams-table").length).toEqual(0);
    });

    it("shows an error message in h2", () => {
        expect(wrapped.find(".error-message").text()).toBe(
            "No data available currently!"
        );
    });
});
