import { mount } from "enzyme";
import React from "react";
import { GoogleAuth } from "../GoogleAuth";

describe("Google Auth component", () => {
    let wrapped, props;
    beforeEach(() => {
        props = {
            auth: false,
            getAuthStatusAction: jest.fn(() => "getAuthStatusAction"),
            mockAuth: jest.fn(() => {
                "mockAuth";
            })
        };

        wrapped = mount(<GoogleAuth {...props} />);
    });

    it("shows a login button with text Sign in with Google", () => {
        expect(wrapped.find(`[data-test="GoogleLoginButton"]`).length).toEqual(
            1
        );
        expect(wrapped.find(`[data-test="GoogleLoginButton"]`).text()).toEqual(
            "Sign in with Google"
        );
    });

    // it("calls getAuthStatus action on clicking logging in", () => {
    //     wrapped.find(`[data-test="GoogleLoginButton"]`).simulate("click");
    //     expect(GoogleAuth.onGoogleAuthClick).toBeCalled();
    // });
});
