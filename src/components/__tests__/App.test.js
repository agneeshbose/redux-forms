import React from "react";
import { shallow } from "enzyme";
import App from "../App";

describe("App component", () => {
  //
  it("shows Header and StreamList components", done => {
    const wrapped = shallow(<App />);

    expect(wrapped.find("Header").length).toEqual(1);
    setTimeout(() => {
      expect(wrapped.find("ConnectFunction").length).toEqual(1);

      done();
    }, 1000);
  });

  //
  // it("shows modal dialogue box for creating new stream on Add Stream button click", done => {
  //   const wrapped = shallow(<App />);
  //   setTimeout(() => {
  //     wrapped.find(".addstream-button").simulate("click");
  //     wrapped.update();
  //     expect(wrapped.find("AddStreamModal").length).toEqual(10);
  //     done();
  //     // expect(wrapped.find(".ui.modal.header").text()).toContain(".......");
  //   });
  // }, 1000);
});
