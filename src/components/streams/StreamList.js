import React, { Component } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import {
    getStreamsListAction,
    deleteStreamAction
} from "../actions/streamsAction";
import AddStream from "./AddStream";
import DeleteStreamModal from "../Modal/DeleteStreamModal";
import requireAuth from "../HOC/requireAuth";

export class StreamList extends Component {
    state = {
        modalStatus: false,
        selectedStream: {}
    };

    //Getting intial streams list
    componentDidMount() {
        this.props.getStreamsListAction();
    }
    //

    // Hiding modal dialogue box on clicking cancel
    handleClickCancel = () => {
        this.setState({ modalStatus: false });
    };
    //

    //Deleting selected stream from collection on delete confirmation
    handleDeleteStream = id => {
        this.props.deleteStreamAction(id);
        this.handleClickCancel();
    };
    //

    //Showing up modal on delete button click and passing in selected stream
    handleDeleteButtonClick = stream => {
        this.setState({
            modalStatus: true,
            selectedStream: stream
        });
    };
    //

    //Helper method to display streams table / display error message in case of any issue
    renderStreamsList = () => {
        const streams =
            this.props.streamsList.error === "error" ? (
                <div
                    className="ui segment error-message"
                    style={{ textAlign: "center" }}
                >
                    <h2>No data available currently!</h2>
                </div>
            ) : (
                <div>
                    <AddStream className="addStreamComponent" />
                    <DeleteStreamModal
                        modalStatus={this.state.modalStatus}
                        selectedStream={this.state.selectedStream}
                        onClickCancel={this.handleClickCancel}
                        onDeleteStream={this.handleDeleteStream}
                    />
                    <div>
                        <table className="ui streams-table fixed single line celled table">
                            <thead>
                                <tr>
                                    <th>Stream ID</th>
                                    <th>Name</th>
                                    <th>Duration</th>
                                </tr>
                            </thead>
                            <tbody>
                                {Object.keys(this.props.streamsList).map(
                                    streamId => {
                                        const stream = this.props.streamsList[
                                            streamId
                                        ];
                                        return (
                                            <tr
                                                className="data-row"
                                                key={streamId}
                                            >
                                                <td>{stream.id}</td>
                                                <td>{stream.name}</td>
                                                <td>
                                                    {stream.duration} minutes{" "}
                                                    <button
                                                        className="ui deletestream-button negative basic button right floated"
                                                        onClick={() => {
                                                            this.handleDeleteButtonClick(
                                                                stream
                                                            );
                                                        }}
                                                    >
                                                        Delete Stream{" "}
                                                        <i
                                                            className="shopping basket icon"
                                                            style={{
                                                                marginLeft:
                                                                    "5px"
                                                            }}
                                                        />
                                                    </button>
                                                </td>
                                            </tr>
                                        );
                                    }
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        return streams;
    }; //

    render() {
        return this.renderStreamsList();
    }
}

const mapStateToProps = state => {
    return {
        streamsList: state.streamsList
    };
};

export default connect(
    mapStateToProps,
    { getStreamsListAction, deleteStreamAction }
)(requireAuth(StreamList));
