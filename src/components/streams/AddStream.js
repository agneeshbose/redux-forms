import React, { Component } from "react";
import AddStreamModal from "../Modal/AddStreamModal";
import { connect } from "react-redux";
import { addNewStreamAction } from "../actions/streamsAction";

export class AddStream extends Component {
    state = {
        modalStatus: false
    };

    //Making modal visible
    enableAddStreamModal = () => {
        this.setState({ modalStatus: true });
    };
    //

    // Hiding modal
    closeAddStreamModal = () => {
        this.setState({ modalStatus: false });
    };
    //

    // Updating new stream data to collection via calling action
    onSubmitModal = newStream => {
        this.props.addNewStreamAction(newStream);
    };
    //

    render() {
        return (
            <div>
                <button
                    className="ui primary addstream-button basic button right floated"
                    onClick={this.enableAddStreamModal}
                    style={{ margin: "25px" }}
                >
                    Add Stream <i className="plus icon" />
                </button>
                <AddStreamModal
                    modalStatus={this.state.modalStatus}
                    onClickCancel={this.closeAddStreamModal}
                    onSubmitModal={this.onSubmitModal}
                />
            </div>
        );
    }
}

export default connect(
    null,
    { addNewStreamAction }
)(AddStream);
