import React, { Component } from "react";
import GoogleAuth from "./GoogleAuth";

class Header extends Component {
  render() {
    return (
      <div
        className="ui small menu"
        style={{ marginTop: "20px", padding: "15px" }}
      >
        <div className="item">
          <span className="ui large header">STREAMY </span>
        </div>
        <div className="right menu">
          <div className="item">
            <div>
              <GoogleAuth data-test="auth-button" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
