import React, { Component } from "react";
import { getAuthStatusAction } from "./actions/authAction";
import { connect } from "react-redux";
import gapi from "gapi-client";
import { clientId } from "../client_id";

export class GoogleAuth extends Component {
    componentDidMount() {
        gapi.load("client:auth2", () => {
            gapi.auth2.init({
                clientId: clientId,
                scope: "email"
            });
            this.auth = gapi.auth2.getAuthInstance();
            this.isSignedIn = this.auth.isSignedIn.get();
            this.props.getAuthStatusAction(this.isSignedIn);
        });
    }

    onGoogleAuthClick = () => {
        const isSignedIn = this.props.auth;
        if (isSignedIn === false) {
            this.auth.signIn().then(() => {
                this.props.getAuthStatusAction(this.auth.isSignedIn.get());
            });
        } else if (isSignedIn) {
            this.auth.signOut().then(() => {
                this.props.getAuthStatusAction(this.auth.isSignedIn.get());
            });
        }
    };

    renderButtonLabel = () => {
        if (this.props.auth === null) {
            return "Loading..";
        } else if (this.props.auth === true) {
            return "Log out";
        } else {
            return "Sign in with Google";
        }
    };

    render() {
        console.log(this.props.auth);
        return (
            <div
                className="ui labelled red button right floated"
                data-test="GoogleLoginButton"
                onClick={this.onGoogleAuthClick}
            >
                <i className="google icon" />
                {this.renderButtonLabel()}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(
    mapStateToProps,
    { getAuthStatusAction }
)(GoogleAuth);
