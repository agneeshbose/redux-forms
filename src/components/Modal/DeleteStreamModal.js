import React from "react";
import { Button, Modal } from "semantic-ui-react";

const DeleteStreamModal = props => {
    //Closing modal box on user click cancel
    const onClickClose = () => {
        props.onClickCancel();
    };
    //

    //Passing new stream data back to calling component to update collection
    const onClickDeleteStream = () => {
        props.onDeleteStream(props.selectedStream.id);
        onClickClose();
    };
    //

    // Modal box prompting for delete confirmation
    return (
        <Modal
            open={props.modalStatus}
            closeOnEscape={true}
            closeOnDimmerClick={true}
        >
            <Modal.Header>Create Stream</Modal.Header>
            <Modal.Content image>
                <Modal.Description>
                    <h4>
                        Are you sure to delete stream "
                        {`${props.selectedStream.name}`}" ?
                    </h4>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    onClick={onClickClose}
                    className="cancel-delete-button"
                    primary
                >
                    Cancel
                </Button>
                <Button
                    className="confirm-delete"
                    onClick={onClickDeleteStream}
                    negative
                    labelPosition="right"
                    icon="checkmark"
                    content="Delete"
                />
            </Modal.Actions>
        </Modal>
    );
    //
};

export default DeleteStreamModal;
