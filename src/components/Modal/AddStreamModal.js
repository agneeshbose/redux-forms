import React, { Component } from "react";
import { Button, Modal } from "semantic-ui-react";

class AddStreamModal extends Component {
    // Setting initial state
    initialState = {
        newStream: {
            name: "",
            duration: ""
        },
        formErrors: {
            name: "",
            duration: ""
        }
    };
    //

    state = this.initialState;

    //Updating state with user input
    onTextChange = e => {
        this.setState({
            newStream: {
                ...this.state.newStream,
                [e.target.name]: e.target.value
            }
        });
    };
    //

    //Closing modal box on user click cancel
    onClickClose = () => {
        this.props.onClickCancel();
        this.setState(this.initialState);
    };
    //

    //Passing new stream data back to calling component to update collection
    onClickCreateStream = () => {
        this.props.onSubmitModal(this.state.newStream);
        this.onClickClose();
    };
    //

    render() {
        return (
            <div className="addstream-modal">
                <Modal
                    open={this.props.modalStatus}
                    closeOnEscape={true}
                    closeOnDimmerClick={true}
                >
                    <Modal.Header>Create Stream</Modal.Header>
                    <Modal.Content image>
                        <Modal.Description>
                            <form className="ui form">
                                <div className="two fields">
                                    <div className="field">
                                        <label>Stream Name </label>
                                        <input
                                            placeholder="Enter stream name"
                                            type="text"
                                            name="name"
                                            onChange={this.onTextChange}
                                        />
                                    </div>
                                    <div className="field">
                                        <label>Duration (in mins) </label>
                                        <input
                                            placeholder="Enter duration"
                                            type="text"
                                            name="duration"
                                            onChange={this.onTextChange}
                                        />
                                    </div>
                                </div>
                            </form>
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button
                            className="cancelButton"
                            onClick={this.onClickClose}
                            negative
                        >
                            Cancel
                        </Button>
                        <Button
                            id="test"
                            data-test="createstream-button"
                            onClick={this.onClickCreateStream}
                            positive
                            labelPosition="right"
                            icon="checkmark"
                            content="Create"
                        />
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}

export default AddStreamModal;
