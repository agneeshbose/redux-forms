import React from "react";
import { shallow } from "enzyme";
import AddStreamModal from "../AddStreamModal";

describe("AddStreamModal box", () => {
    let wrapped, props;
    beforeEach(() => {
        props = {
            modalStatus: true,
            onClickCancel: jest.fn(() => "handleModalClickClose"),
            onSubmitModal: jest.fn(() => "handleModalClickSubmit")
        };
        wrapped = shallow(<AddStreamModal {...props} />);
    });

    it("calls onClickClose method on clicking cancel button", () => {
        wrapped.find(".cancelButton").simulate("click");
        expect(props.onClickCancel).toBeCalled();
    });

    it("calls onClickSubmit on clicking submit form", () => {
        wrapped.find(`[name="name"]`).simulate("change", {
            target: { name: "name", value: "new name" }
        });
        wrapped.find(`[name="duration"]`).simulate("change", {
            target: { name: "duration", value: "new duration" }
        });
        wrapped.find(`[data-test="createstream-button"]`).simulate("click");
        expect(props.onSubmitModal).toBeCalledWith({
            name: "new name",
            duration: "new duration"
        });
    });
});
