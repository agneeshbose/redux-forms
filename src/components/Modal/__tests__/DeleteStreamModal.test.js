import { shallow } from "enzyme";
import React from "react";
import DeleteStreamModal from "../DeleteStreamModal";

describe("DeleteStreamModal", () => {
    let wrapped, props;

    beforeEach(() => {
        props = {
            modalStatus: true,
            onClickCancel: jest.fn(() => "onClickClose"),
            onDeleteStream: jest.fn(() => "onClickDeleteStream"),
            selectedStream: { name: "stream" }
        };
        wrapped = shallow(<DeleteStreamModal {...props} />);
    });

    it("calls onClickCancel prop while clicking cancel delete", () => {
        wrapped.find(".cancel-delete-button").simulate("click");
        expect(props.onClickCancel).toBeCalled();
    });

    it("calls onDeleteStream prop while clicking delete button", () => {
        wrapped.find(".confirm-delete").simulate("click");
        expect(props.onDeleteStream).toBeCalled();
    });
});
