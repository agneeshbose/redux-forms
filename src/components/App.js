import React from "react";
import Header from "./Header";
import StreamList from "./streams/StreamList";

class App extends React.Component {
    render() {
        return (
            <div className="ui container">
                <Header />
                <StreamList />
            </div>
        );
    }
}

export default App;
