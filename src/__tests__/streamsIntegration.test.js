import moxios from "moxios";
import reducers from "../components/reducers/index";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import {
    deleteStreamAction,
    addNewStreamAction,
    getStreamsListAction
} from "../components/actions/streamsAction";
import _ from "lodash";

describe("Integration tests for Streams", () => {
    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    // Test - 1

    it("calls api and gets streams data from collection", done => {
        // Initial store state to be passed
        const streams = {
            streamsList: {
                2: { id: 2, name: "Arun", duration: "30" },
                5: { id: 5, name: "Thomas", duration: "35" },
                6: { id: 6, name: "Tom", duration: "34" }
            }
        };
        //

        // Store to test actions
        const store = createStore(reducers, applyMiddleware(thunk));
        //

        // Mocking axios response
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: [
                    {
                        id: 2,
                        name: "Arun",
                        duration: "30"
                    },
                    {
                        id: 5,
                        name: "Thomas",
                        duration: "35"
                    },
                    {
                        id: 6,
                        name: "Tom",
                        duration: "34"
                    }
                ]
            });
        });
        //

        // Dispatching action
        store.dispatch(getStreamsListAction()).then(() => {
            const newStore = store.getState();
            expect(newStore.streamsList).toEqual(streams.streamsList);
            done();
        });
    });

    //

    // Test - 2

    it("calls api and adds new stream data to collection", done => {
        // Initial store state to be passed
        const initialState = {
            streamsList: {
                2: { id: 2, name: "Arun", duration: "30" },
                5: { id: 5, name: "Thomas", duration: "35" }
            }
        };
        //

        // Store to test actions
        const store = createStore(
            reducers,
            initialState,
            applyMiddleware(thunk)
        );
        //

        // Mocking axios response
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 201,
                response: { id: "6", name: "Tom", duration: "34", header: {} }
            });
        });
        //

        // Dispatching action
        store
            .dispatch(addNewStreamAction({ name: "Tom", duration: "34" }))
            .then(() => {
                const newStream = {
                    6: { id: "6", name: "Tom", duration: "34" }
                };
                const newStore = store.getState();
                expect(newStore.streamsList).toEqual({
                    ...initialState.streamsList,
                    ...newStream
                });
                done();
            });
    });

    //Test - 3

    it("calls api and delete selected stream from collection", done => {
        // Initial store state to be passed
        const initialState = {
            streamsList: {
                2: { id: "2", name: "Arun", duration: "30" },
                5: { id: "5", name: "Thomas", duration: "35" },
                6: { id: "6", name: "Tom", duration: "34" }
            }
        };
        //

        // Store to test actions
        const store = createStore(
            reducers,
            initialState,
            applyMiddleware(thunk)
        );
        //

        // Mocking axios response
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                data: {}
            });
        });
        //

        // Dispatching action
        store.dispatch(deleteStreamAction(2)).then(() => {
            const newStore = store.getState();
            expect(newStore.streamsList).toEqual(
                _.omit(initialState.streamsList, [2])
            );
            done();
        });
    });

    // Test - 4

    it("calls api to get streams but errors", done => {
        // Store to test actions
        const store = createStore(reducers, applyMiddleware(thunk));
        //

        // Mocking axios response
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.reject({ status: 500 });
        });
        //

        // Dispatching action
        store.dispatch(getStreamsListAction()).then(() => {
            const newStore = store.getState();
            expect(newStore.streamsList.error).toEqual("error");
            done();
        });
    });
});
