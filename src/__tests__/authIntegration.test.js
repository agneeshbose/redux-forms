import { getAuthStatusAction } from "../components/actions/authAction"
import reducers from "../components/reducers/index";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

describe("Auth integration", () => {

    it("change auth status", () => {
        // Store to test actions
        const store = createStore(reducers, applyMiddleware(thunk));
        //

        store.dispatch(getAuthStatusAction(true));
        const newStore = store.getState();
        expect(newStore.auth).toEqual(true);
    })
})