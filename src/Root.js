import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import reducers from "./components/reducers/index";
import thunk from "redux-thunk";

const composeEnhancers =
    typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));

export const store = initialState =>
    createStore(reducers, initialState, enhancer);

const Root = ({ children, initialState = {} }) => {
    return <Provider store={store(initialState)}>{children}</Provider>;
};

export default Root;
