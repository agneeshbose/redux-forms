import axios from "axios";

//api call to get the initial stream list
export const getStreamsList = () => {
  return axios.get("http://localhost:3001/streams");
};
//

//api call to add new stream to collection
export const addNewStream = ({ name, duration }) => {
  return axios.post("http://localhost:3001/streams", {
    header: {
      contentType: "application/json"
    },
    name,
    duration
  });
};
//

//api call to delete stream from collection
export const deleteStream = id => {
  return axios.delete(`http://localhost:3001/streams/${id}`);
};
//
