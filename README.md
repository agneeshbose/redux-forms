## A Simple CRUD project using React and Redux

A sample CRUD project which uses React and Redux and a fake json-server as backend database. Included unit tests and integration tests as well.

Clone and type npm start to run the project. Before that, ensure db.json in the server directory is up and running as a backend db.

For that please run the below command in the server directory.

### `json-server --watch db.json --port 3001`

Please make sure that you are running the json-server on port 3001.
